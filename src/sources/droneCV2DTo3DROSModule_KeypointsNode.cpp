//////////////////////////////////////////////////////
//  DroneCVIARC14ROSModule_KeypointsNode.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//cv module
#include "droneCV2DTo3DROSModule.h"


//Nodes names
//MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR
#include "nodes_definition.h"





using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, MODULE_NAME_DRONE_CV_KEYPOINTS_2D_TO_3D); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();
    cout<<"Starting "<<node_name<<endl;

    //Class
    DroneCVKeypoints2DTo3DROSModule MyDroneCVKeypoints2DTo3DROSModule;


    //Open
    MyDroneCVKeypoints2DTo3DROSModule.open(n);


    //Loop -> Ashyncronous Module
    ros::spin();

    return 1;
}

