//////////////////////////////////////////////////////
//  droneCV2DTo3DROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneCV2DTo3DROSModule.h"



using namespace std;



///////////////////////////////////////////
////////////////// DroneCV2DTo3DROS ////////////////////
/////////////////////////////////////////
DroneCV2DTo3DROS::DroneCV2DTo3DROS()
{
    dronePoseSetFlag=false;
    rotationAnglesFlag=false;
    return;
}
DroneCV2DTo3DROS::~DroneCV2DTo3DROS()
{
    return;
}



int DroneCV2DTo3DROS::setCalibCamera(std::string camCalibParamFile)
{
    this->camCalibParamFile=camCalibParamFile;
    return 1;
}

int DroneCV2DTo3DROS::setCameraPoseWRTDrone(std::string camPoseWRTDroneFile)
{
    this->camPoseWRTDroneFile=camPoseWRTDroneFile;
    return 1;
}

int DroneCV2DTo3DROS::setDronePoseTopicConfigs(std::string dronePoseTopicName, std::string droneRotationAnglesTopicName)
{
    this->dronePoseTopicName=dronePoseTopicName;
    this->droneRotationAnglesTopicName=droneRotationAnglesTopicName;
    return 1;
}

void DroneCV2DTo3DROS::dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg)
{
    //TODO poner un buffer
    dronePoseMsg=*msg;

    dronePoseSetFlag=true;
    return;
}

void DroneCV2DTo3DROS::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
    //TODO poner un buffer
    droneRotationAnglesMsg=*msg;

    //Change a rad
    droneRotationAnglesMsg.vector.x=droneRotationAnglesMsg.vector.x*M_PI/180.0;
    droneRotationAnglesMsg.vector.y=droneRotationAnglesMsg.vector.y*M_PI/180.0;
    droneRotationAnglesMsg.vector.z=droneRotationAnglesMsg.vector.z*M_PI/180.0;

    rotationAnglesFlag=true;

    return;
}


void DroneCV2DTo3DROS::open(ros::NodeHandle & nIn)
{
    //Subscribers com
    dronePoseSubs = nIn.subscribe(dronePoseTopicName, 1, &DroneCVKeypoints2DTo3DROSModule::dronePoseCallback, this);
    droneRotationAnglesSubs = nIn.subscribe(droneRotationAnglesTopicName, 1, &DroneCVKeypoints2DTo3DROSModule::droneRotationAnglesCallback, this);

    return;
}



///////////////////////////////////////////
////////////////// DroneCVKeypoints2DTo3DROSModule ////////////////////
/////////////////////////////////////////
DroneCVKeypoints2DTo3DROSModule::DroneCVKeypoints2DTo3DROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneCVKeypoints2DTo3DROSModule::~DroneCVKeypoints2DTo3DROSModule()
{
	close();
	return;
}


bool DroneCVKeypoints2DTo3DROSModule::init()
{
    DroneModule::init();


    /////Init cv module
    //Camera parameters
    if(!MyDroneCV2DTo3D.setCameraCalibParameters(camCalibParamFile))
    {
        cout<<"Error"<<endl;
        return false;
    }

    //Camera pose wrt drone
    if(!MyDroneCV2DTo3D.setCameraPoseWRTDrone(camPoseWRTDroneFile))
    {
        cout<<"Error"<<endl;
        return false;
    }




    //end
    return true;
}

int DroneCVKeypoints2DTo3DROSModule::setKeypoints2DTopicName(std::string keypoints2DTopicName)
{
    this->keypoints2DTopicName=keypoints2DTopicName;
    return 1;
}

void DroneCVKeypoints2DTo3DROSModule::keypoints2DCallback(const droneMsgsROS::vectorPoints2DInt::ConstPtr& msg)
{
    //copy msg
    keypoints2DMsg=*msg;

    //Run -> asynchronous
    if(!run())
    {
        cout<<"[droneModule] error running"<<endl;
        return;
    }

    return;
}


void DroneCVKeypoints2DTo3DROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);


    //Camera parameters
    std::string camera_calib_param;
    ros::param::get("~camera_calib_param", camera_calib_param);
    cout<<"camera_calib_param="<<camera_calib_param<<endl;
    if ( camera_calib_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera calibration paramaters"<<endl;
    }

    //Camera pose wrt drone
    std::string camera_pose_wrt_drone;
    ros::param::get("~camera_pose_wrt_drone", camera_pose_wrt_drone);
    cout<<"camera_pose_wrt_drone="<<camera_pose_wrt_drone<<endl;
    if ( camera_pose_wrt_drone.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera_pose_wrt_drone"<<endl;
    }

    //dronePose subscription topic
    std::string drone_pose_topic_name;
    ros::param::get("~drone_pose_topic_name",drone_pose_topic_name);
    cout<<"drone_pose_topic_name="<<drone_pose_topic_name<<endl;
    if (drone_pose_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with drone_pose_topic_name";
    }

    //droneRotationAngles subscription topic
    std::string drone_rotation_angles_topic_name;
    ros::param::get("~drone_rotation_angles_topic_name",drone_rotation_angles_topic_name);
    cout<<"drone_rotation_angles_topic_name="<<drone_rotation_angles_topic_name<<endl;
    if (drone_rotation_angles_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video drone_rotation_angles_topic_name";
    }

    //keypoints 2d subscription topic
    std::string keypoints_2d_topic_name;
    ros::param::get("~keypoints_2d_topic_name",keypoints_2d_topic_name);
    cout<<"keypoints_2d_topic_name="<<keypoints_2d_topic_name<<endl;
    if (keypoints_2d_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with keypoints_2d_topic_name";
    }

    //keypoints3D publi topic
    std::string keypoints_3d_topic_name;
    ros::param::get("~keypoints_3d_topic_name",keypoints_3d_topic_name);
    cout<<"keypoints_3d_topic_name="<<keypoints_3d_topic_name<<endl;
    if (keypoints_3d_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video keypoints_3d_topic_name";
    }


    //Configs
    if(!setCalibCamera(camera_calib_param))
        return;

    if(!setCameraPoseWRTDrone(camera_pose_wrt_drone))
        return;

    if(!setDronePoseTopicConfigs(drone_pose_topic_name,drone_rotation_angles_topic_name))
        return;

    if(!setKeypoints2DTopicName(keypoints_2d_topic_name))
        return;

    if(!setKeypoints3DPublTopicName(keypoints_3d_topic_name))
        return;


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }

    //Open Com topics
    DroneCV2DTo3DROS::open(nIn);


    //// TOPICS
    //Subscribers
    keypoints2DSubs = n.subscribe(keypoints2DTopicName, 1, &DroneCVKeypoints2DTo3DROSModule::keypoints2DCallback, this);



    //Publishers
    droneKeypoints3DPubl=n.advertise<droneMsgsROS::points3DStamped>(keypoints3DPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

	
	//End
	return;
}


void DroneCVKeypoints2DTo3DROSModule::close()
{
    DroneModule::close();

    //Do stuff

    return;
}


bool DroneCVKeypoints2DTo3DROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneCVKeypoints2DTo3DROSModule::startVal()
{
    //Do stuff

    //End
    return DroneModule::startVal();
}


bool DroneCVKeypoints2DTo3DROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneCVKeypoints2DTo3DROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    //Set pose of the drone
    if(!rotationAnglesFlag || !dronePoseSetFlag)
        return false;

    //TODO use a buffer
    SL::Pose dronePose;
    std::vector<double> position(3), attitude(3);

    //TODO revisar!!!
    //Position
    position[0]=0.0;//dronePoseMsg.x;
    position[1]=0.0;//dronePoseMsg.y;
    position[2]=dronePoseMsg.z;
    //Attitude
    attitude[0]=0.0;//dronePoseMsg.yaw; //yaw
    attitude[1]=(-1.0)*droneRotationAnglesMsg.vector.y; //pitch
    attitude[2]=droneRotationAnglesMsg.vector.x; //roll

    if(!dronePose.setDroneNavData(position,attitude))
        return false;

    if(!MyDroneCV2DTo3D.setDronePose(dronePose))
        return false;


    //Calculate projection of points
    pointsIn3DWorld.clear();
    for(std::vector<droneMsgsROS::vector2i>::const_iterator it = keypoints2DMsg.point2DInt.begin() ; it != keypoints2DMsg.point2DInt.end(); ++it)
    {
        //Set point2D
        cv::Point pointInImagePlane;
        pointInImagePlane.x=it->x;
        pointInImagePlane.y=it->y;
        if(!MyDroneCV2DTo3D.setPoint2D(pointInImagePlane))
            return false;

        //run
        if(!MyDroneCV2DTo3D.run())
            return false;

        //get point3D
        cv::Point3d pointIn3DWorld;
        if(!MyDroneCV2DTo3D.getPoint3D(pointIn3DWorld))
            return false;

        //cout<<" pointIn3DWorld"<<pointIn3DWorld<<endl;
        //push
        pointsIn3DWorld.push_back(pointIn3DWorld);

    }


    //Publish
    if(!publishKeypoints3D())
    {
        cout<<"[droneModule] error publishing"<<endl;
        return false;
    }

    return true;
}

int DroneCVKeypoints2DTo3DROSModule::setKeypoints3DPublTopicName(std::string keypoints3DPublTopicName)
{
    this->keypoints3DPublTopicName=keypoints3DPublTopicName;
    return 1;
}


bool DroneCVKeypoints2DTo3DROSModule::publishKeypoints3D()
{
    if(droneModuleOpened==false)
        return false;


    //Transform to message
    droneMsgsROS::points3DStamped keypoints3DMsg;
    //Header. Time of the image
    keypoints3DMsg.header.stamp=keypoints2DMsg.header.stamp;
    //Points
    keypoints3DMsg.points3D.clear();
    for(std::vector<cv::Point3d>::iterator it=pointsIn3DWorld.begin(); it!=pointsIn3DWorld.end(); ++it)
    {
        droneMsgsROS::vector3f oneKeypoint3D;

        oneKeypoint3D.x=it->x;
        oneKeypoint3D.y=it->y;
        oneKeypoint3D.z=it->z;

        keypoints3DMsg.points3D.push_back(oneKeypoint3D);
    }


    //Publish
    droneKeypoints3DPubl.publish(keypoints3DMsg);


    return true;

}









///////////////////////////////////////////
////////////////// DroneCVGroundRobots2DTo3DROSModule ////////////////////
/////////////////////////////////////////
DroneCVGroundRobots2DTo3DROSModule::DroneCVGroundRobots2DTo3DROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneCVGroundRobots2DTo3DROSModule::~DroneCVGroundRobots2DTo3DROSModule()
{
    close();
    return;
}

bool DroneCVGroundRobots2DTo3DROSModule::init()
{
    DroneModule::init();


    /////Init cv module
    //Camera parameters
    if(!MyDroneCV2DTo3D.setCameraCalibParameters(camCalibParamFile))
    {
        cout<<"Error"<<endl;
        return false;
    }

    //Camera pose wrt drone
    if(!MyDroneCV2DTo3D.setCameraPoseWRTDrone(camPoseWRTDroneFile))
    {
        cout<<"Error"<<endl;
        return false;
    }




    //end
    return true;
}

int DroneCVGroundRobots2DTo3DROSModule::setGroundRobots2DTopicName(std::string groundRobots2DTopicName)
{
    this->groundRobots2DTopicName=groundRobots2DTopicName;
    return 1;
}

void DroneCVGroundRobots2DTo3DROSModule::groundRobots2DCallback(const droneMsgsROS::vectorTargetsInImageStamped::ConstPtr& msg)
{
    if(!moduleStarted)
        return;

    //copy msg
    groundRobots2DMsg=*msg;

    //Run -> asynchronous
    if(!run())
    {
        cout<<"[droneModule] error running"<<endl;
        return;
    }

    return;
}


void DroneCVGroundRobots2DTo3DROSModule::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);


    //Camera parameters
    std::string camera_calib_param;
    ros::param::get("~camera_calib_param", camera_calib_param);
    cout<<"camera_calib_param="<<camera_calib_param<<endl;
    if ( camera_calib_param.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera calibration paramaters"<<endl;
    }

    //Camera pose wrt drone
    std::string camera_pose_wrt_drone;
    ros::param::get("~camera_pose_wrt_drone", camera_pose_wrt_drone);
    cout<<"camera_pose_wrt_drone="<<camera_pose_wrt_drone<<endl;
    if ( camera_pose_wrt_drone.length() == 0)
    {
        cout<<"[ROSNODE] Error with camera_pose_wrt_drone"<<endl;
    }

    //dronePose subscription topic
    std::string drone_pose_topic_name;
    ros::param::get("~drone_pose_topic_name",drone_pose_topic_name);
    cout<<"drone_pose_topic_name="<<drone_pose_topic_name<<endl;
    if (drone_pose_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with drone_pose_topic_name";
    }

    //droneRotationAngles subscription topic
    std::string drone_rotation_angles_topic_name;
    ros::param::get("~drone_rotation_angles_topic_name",drone_rotation_angles_topic_name);
    cout<<"drone_rotation_angles_topic_name="<<drone_rotation_angles_topic_name<<endl;
    if (drone_rotation_angles_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video drone_rotation_angles_topic_name";
    }

    //ground robots 2d subscription topic
    std::string ground_robots_2d_topic_name;
    ros::param::get("~ground_robots_2d_topic_name",ground_robots_2d_topic_name);
    cout<<"ground_robots_2d_topic_name="<<ground_robots_2d_topic_name<<endl;
    if (ground_robots_2d_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with ground_robots_2d_topic_name";
    }

    //groundRobots3D publi topic
    std::string ground_robots_3d_topic_name;
    ros::param::get("~ground_robots_3d_topic_name",ground_robots_3d_topic_name);
    cout<<"ground_robots_3d_topic_name="<<ground_robots_3d_topic_name<<endl;
    if (ground_robots_3d_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video ground_robots_3d_topic_name";
    }

    //Configs
    if(!setCalibCamera(camera_calib_param))
        return;

    if(!setCameraPoseWRTDrone(camera_pose_wrt_drone))
        return;

    if(!setDronePoseTopicConfigs(drone_pose_topic_name,drone_rotation_angles_topic_name))
        return;

    if(!setGroundRobots2DTopicName(ground_robots_2d_topic_name))
        return;

    if(!setGroundRobots3DPublTopicName(ground_robots_3d_topic_name))
        return;



    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }

    //Open Com topics
    DroneCV2DTo3DROS::open(nIn);


    //// TOPICS
    //Subscribers
    groundRobots2DSubs = n.subscribe(groundRobots2DTopicName, 1, &DroneCVGroundRobots2DTo3DROSModule::groundRobots2DCallback, this);



    //Publishers
    droneGroundRobots3DPubl=n.advertise<droneMsgsROS::robotPoseVector>(groundRobots3DPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;


    //End
    return;
}


void DroneCVGroundRobots2DTo3DROSModule::close()
{
    DroneModule::close();

    //Do stuff

    return;
}


bool DroneCVGroundRobots2DTo3DROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneCVGroundRobots2DTo3DROSModule::startVal()
{
    //Do stuff

    //End
    return DroneModule::startVal();
}


bool DroneCVGroundRobots2DTo3DROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneCVGroundRobots2DTo3DROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    //Set pose of the drone
    //TODO use a buffer
    SL::Pose dronePose;
    std::vector<double> position(3), attitude(3);

    //TODO revisar!!!
    //Position
    position[0]=0.0;//dronePoseMsg.x;
    position[1]=0.0;//dronePoseMsg.y;
    position[2]=dronePoseMsg.z;
    //Attitude
    attitude[0]=0.0;//dronePoseMsg.yaw; //yaw
    attitude[1]=(-1.0)*droneRotationAnglesMsg.vector.y; //pitch
    attitude[2]=droneRotationAnglesMsg.vector.x; //roll

    if(!dronePose.setDroneNavData(position,attitude))
        return false;

    if(!MyDroneCV2DTo3D.setDronePose(dronePose))
        return false;


    //Calculate projection of points
    pointsIn3DWorld.clear();
    for(std::vector<droneMsgsROS::targetInImage>::const_iterator it = groundRobots2DMsg.targetsInImage.begin() ; it != groundRobots2DMsg.targetsInImage.end(); ++it)
    {
        //Set point2D
        cv::Point pointInImagePlane;

        //Calculate center of bounding box
        //TODO revisar!!!
        pointInImagePlane.x=it->x+it->width/2;
        pointInImagePlane.y=it->y+it->height/2;

        //cout<<"pointInImagePlane"<<pointInImagePlane<<endl;


        //cv::Mat test(720,480,CV_32F);
        //circle(test,cv::Point(pointInImagePlane.x,pointInImagePlane,y))

        if(!MyDroneCV2DTo3D.setPoint2D(pointInImagePlane))
            return false;

        //run
        if(!MyDroneCV2DTo3D.run())
            return false;

        //get point3D
        cv::Point3d pointIn3DWorld;
        if(!MyDroneCV2DTo3D.getPoint3D(pointIn3DWorld))
            return false;

        //push
        pointsIn3DWorld.push_back(pointIn3DWorld);

    }


    //Publish
    if(!publishGroundRobots3D())
    {
        cout<<"[droneModule] error publishing"<<endl;
        return false;
    }

    return true;
}

int DroneCVGroundRobots2DTo3DROSModule::setGroundRobots3DPublTopicName(std::string groundRobots3DPublTopicName)
{
    this->groundRobots3DPublTopicName=groundRobots3DPublTopicName;
    return 1;
}


bool DroneCVGroundRobots2DTo3DROSModule::publishGroundRobots3D()
{
    if(droneModuleOpened==false)
        return false;


    //Transform to message
    droneMsgsROS::robotPoseVector droneGroundRobots3DMsg;
    //Header. Time of the image
    droneGroundRobots3DMsg.header.stamp=groundRobots2DMsg.header.stamp;
    //Robots
    droneGroundRobots3DMsg.robot_pose_vector.clear();
    std::vector<cv::Point3d>::iterator it1; //Points processed
    std::vector<droneMsgsROS::targetInImage>::iterator it2; //Msg input

    //cout<<"1"<<endl;
    for(it1=pointsIn3DWorld.begin(), it2=groundRobots2DMsg.targetsInImage.begin(); it1!=pointsIn3DWorld.end(), it2!=groundRobots2DMsg.targetsInImage.end(); ++it1, ++it2)
    {
        //cout<<"1"<<endl;
        droneMsgsROS::robotPose oneRobotPose3D;

        oneRobotPose3D.x=it1->x;
        oneRobotPose3D.y=it1->y;
        oneRobotPose3D.z=it1->z;

        oneRobotPose3D.theta=0.0; //unknown

        oneRobotPose3D.id_Robot=it2->id;

        oneRobotPose3D.Robot_Type=it2->type;

        droneGroundRobots3DMsg.robot_pose_vector.push_back(oneRobotPose3D);
    }


    //Publish
    droneGroundRobots3DPubl.publish(droneGroundRobots3DMsg);


    return true;

}


