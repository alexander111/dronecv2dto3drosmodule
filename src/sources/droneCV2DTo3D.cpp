//////////////////////////////////////////////////////
//  droneCV2DTo3D.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneCV2DTo3D.h"



using namespace std;


int DroneCV2DTo3D::setCameraCalibParameters(std::string fileName)
{

    cv::FileStorage fs(fileName, cv::FileStorage::READ);
    int w=-1,h=-1;
    cv::Mat MCamera;


    fs["image_width"] >> w;
    fs["image_height"] >> h;
    fs["sensor_width"] >> this->SensorWidth;
    fs["sensor_height"] >> this->SensorHeight;
    fs["camera_matrix"] >> MCamera;

    if (MCamera.cols==0 || MCamera.rows==0)throw cv::Exception(9007,"File :"+fileName+" does not contains valid camera matrix","CameraParameters::readFromXML",__FILE__,__LINE__);
    if (w==-1 || h==0)throw cv::Exception(9007,"File :"+fileName+" does not contains valid camera dimensions","CameraParameters::readFromXML",__FILE__,__LINE__);

    if (MCamera.type()!=CV_32FC1) MCamera.convertTo(CameraMatrix,CV_32FC1);
    else CameraMatrix=MCamera;


    CamSize.width=w;
    CamSize.height=h;


    return 1;
}



int DroneCV2DTo3D::setPoint2D(cv::Point pointInImagePlane)
{
    this->pointInImagePlane=pointInImagePlane;
    return 1;
}


int DroneCV2DTo3D::setDronePose(SL::Pose dronePose)
{
    this->dronePose=dronePose;
    return 1;
}

int DroneCV2DTo3D::getDronePose(SL::Pose& dronePose)
{
    dronePose=this->dronePose;
    return 1;
}


int DroneCV2DTo3D::setCameraPoseWRTDrone(std::string fileName)
{
    cv::FileStorage fs(fileName, cv::FileStorage::READ);
    cv::Mat MCamera, cameraPose_wrt_drone;

    fs["camera_pose_wrt_droneGMR"] >> MCamera;

    if (MCamera.cols==0 || MCamera.rows==0)
        throw cv::Exception(9007,"File :"+fileName+" does not contains valid camera matrix","CameraParameters::readFromXML",__FILE__,__LINE__);

    if (MCamera.type()!=CV_64FC1)
        MCamera.convertTo(cameraPose_wrt_drone,CV_64FC1);
    else
        cameraPose_wrt_drone=MCamera;

    //
    PoseCamera_wrt_Drone.setHomogMat(cameraPose_wrt_drone);


    return 1;
}

int DroneCV2DTo3D::getPoint3D(cv::Point3d& pointIn3DWorld)
{
    pointIn3DWorld=this->pointIn3DWorld;
    return 1;
}

int DroneCV2DTo3D::run()
{

    //TODO
    float pixelSize = this->SensorWidth/this->CamSize.width;
    //cout<<"pixelSize"<<pixelSize<<endl;
    float f=this->CameraMatrix.at<float>(0,0)*pixelSize;
    //cv::Point3d opticalCenterwrtCamera(0,0,-f);

    SL::Pose PoseCamera_wrt_World;

    PoseCamera_wrt_World=dronePose+PoseCamera_wrt_Drone;

    cv::Mat dronePose_wrt_World, poseCamera_wrt_World;
    cv::Mat poseCamera_wrt_Drone;
    dronePose.getHomogMat(dronePose_wrt_World);

    //cout<<"dronePose_wrt_World"<<dronePose_wrt_World<<endl;
    PoseCamera_wrt_Drone.getHomogMat(poseCamera_wrt_Drone);
    PoseCamera_wrt_World.getHomogMat(poseCamera_wrt_World);

    //poseCamera_wrt_World=dronePose_wrt_World*poseCamera_wrt_Drone;

    //cout<<"poseCamera_wrt_World"<<poseCamera_wrt_World<<endl;

    cv::Mat opticalCenter_wrt_Camera(4,1,CV_64FC1);
    opticalCenter_wrt_Camera.at<double>(0,0)=0.0;
    opticalCenter_wrt_Camera.at<double>(1,0)=0.0;
    opticalCenter_wrt_Camera.at<double>(2,0)=-f;
    opticalCenter_wrt_Camera.at<double>(3,0)=1.0;

    //cout<<"f"<<f<<endl;

    cv::Mat opticalCenter_wrt_World=poseCamera_wrt_World*opticalCenter_wrt_Camera;

    //cv::Mat opticalCenter_wrt_Drone = poseCamera_wrt_Drone*opticalCenter_wrt_Camera;

    //cout<<"opticalCenter_wrt_Drone"<<opticalCenter_wrt_Drone<<endl;


    float cx=this->CameraMatrix.at<float>(0,2);
    float cy=this->CameraMatrix.at<float>(1,2);

    cv::Mat robotProjectionCoordinates_wrt_Camera(4,1,CV_64FC1);
    robotProjectionCoordinates_wrt_Camera.at<double>(0,0)=pixelSize*(pointInImagePlane.x - cx);
    robotProjectionCoordinates_wrt_Camera.at<double>(1,0)=pixelSize*(pointInImagePlane.y - cy);
    robotProjectionCoordinates_wrt_Camera.at<double>(2,0)=0.0;
    robotProjectionCoordinates_wrt_Camera.at<double>(3,0)=1.0;


    //cout<<"robotProjectionCoordinates_wrt_Camera"<<robotProjectionCoordinates_wrt_Camera<<endl;
    //cv::Point3d robotProjectionCoordinateswrtWorld = cv::convertPointsFromHomogeneous(poseCamera_wrt_World * convertPointsToHomogeneous(robotProjectionCoordinateswrtCamera));


    cv::Mat robotProjectionCoordinates_wrt_World;

    robotProjectionCoordinates_wrt_World=poseCamera_wrt_World*robotProjectionCoordinates_wrt_Camera;
    //cout<<"robotProjectionCoordinates_wrt_World"<<robotProjectionCoordinates_wrt_World<<endl;

    double t=-opticalCenter_wrt_World.at<double>(2)/(robotProjectionCoordinates_wrt_World.at<double>(2)-opticalCenter_wrt_World.at<double>(2));
    //cout<<"t"<<t<<endl;
    //cv::Point3d intersectionWithFloorwrtWorld(opticalCenterwrtWorld.x+t*(robotProjectionCoordinateswrtWorld.x-opticalCenterwrtWorld.x),opticalCenterwrtWorld.y+t*(robotProjectionCoordinateswrtWorld.y-opticalCenterwrtWorld.y),0);

    cv::Mat intersectionWithFloor_wrt_World(4,1,CV_64FC1);

    intersectionWithFloor_wrt_World.at<double>(0,0)=opticalCenter_wrt_World.at<double>(0,0)+t*(robotProjectionCoordinates_wrt_World.at<double>(0,0)-opticalCenter_wrt_World.at<double>(0,0));
    intersectionWithFloor_wrt_World.at<double>(1,0)=opticalCenter_wrt_World.at<double>(1,0)+t*(robotProjectionCoordinates_wrt_World.at<double>(1,0)-opticalCenter_wrt_World.at<double>(1,0));
    intersectionWithFloor_wrt_World.at<double>(2,0)=0.0;
    intersectionWithFloor_wrt_World.at<double>(3,0)=1.0;


    //this->pointIn3DWorld = cv::convertPointsFromHomogeneous(dronePose_wrt_World.inv() * cv::convertPointsToHomogeneous(intersectionWithFloorwrtWorld));
    cv::Mat point_wrt_World;

    point_wrt_World=dronePose_wrt_World.inv()*intersectionWithFloor_wrt_World;

    //
    this->pointIn3DWorld.x=point_wrt_World.at<double>(0);
    this->pointIn3DWorld.y=point_wrt_World.at<double>(1);
    this->pointIn3DWorld.z=point_wrt_World.at<double>(2);

    //cout<<"pointIn3DWorld="<<pointIn3DWorld<<endl;

    return 1;
}

